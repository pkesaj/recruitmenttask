//
//  Recruitment_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by mac on 23/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

class Recruitment_iOSTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNetworkRequests() {
        let expectation = self.expectation(description: "Testing all requests.")
        var index: Int = 0 {
            didSet {
                if index == FilenameProvider.Filename.allCases.count {
                    expectation.fulfill()
                }
            }
        }

        for filename in FilenameProvider.Filename.allCases {
            switch filename {
            case .items:
                NetworkingManager.provide(filename: filename.rawValue, decodeTo: ItemsModel()) { result in
                    switch result {
                    case .failure(let error):
                        XCTAssert(false, "\(filename.rawValue) result is nil. Error: \(error)")
                    default: break
                    }
                    index += 1
                }
            default:
                NetworkingManager.provide(filename: filename.rawValue, decodeTo: ItemModel(), completion: { result in
                    switch result {
                    case .failure(let error):
                        XCTAssert(false, "\(filename.rawValue) result is nil. Error: \(error)")
                    default: break
                    }
                    index += 1
                })
            }
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
}
