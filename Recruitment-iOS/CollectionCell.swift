//
//  CollectionCell.swift
//  Recruitment-iOS
//
//  Created by mac on 23/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    static let identifier = "CollectionCell"
    
    private weak var titleLabel: UILabel?
    
    var model: Attributes? {
        didSet {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
            }
        }
    }
    
    override var reuseIdentifier: String? {
        return CollectionCell.identifier
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupLabel()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        backgroundColor = model?.color?.getColor()
        titleLabel?.text = model?.name
    }
    
    private func setupLabel() {
        guard titleLabel == nil else { return }
        let label = UILabel()
        self.addSubview(label)
        titleLabel = label
        
        label.text = model?.name
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}

extension CollectionCell {
    
    static func getCellSize(_ collectionView: UICollectionView) -> CGSize {
        let cellWidth = (collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right - ((collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing ?? 0)) / 2
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
}
