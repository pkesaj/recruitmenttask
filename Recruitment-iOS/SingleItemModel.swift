//
//  SingleItemModel.swift
//  Recruitment-iOS
//
//  Created by mac on 22/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct SingleItemModel: Codable {
    var data: ItemModel?
    
    init() {
        self.data = nil
    }
}
