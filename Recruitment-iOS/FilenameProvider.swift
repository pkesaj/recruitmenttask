//
//  FilenameProvider.swift
//  Recruitment-iOS
//
//  Created by mac on 22/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct FilenameProvider {
    
    enum Filename: String, CaseIterable {
        case items = "Items"
        case item1 = "Item1"
        case item2 = "Item2"
        case item3 = "Item3"
        case item4 = "Item4"
        case item5 = "Item5"
    }
}
