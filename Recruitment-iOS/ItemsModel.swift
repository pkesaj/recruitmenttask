//
//  ItemsModel.swift
//  Recruitment-iOS
//
//  Created by mac on 22/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

// MARK: - ItemsModel
struct ItemsModel: Codable {
    
    let data: [ItemModel]?
    
    init() {
        self.data = nil
    }
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}
