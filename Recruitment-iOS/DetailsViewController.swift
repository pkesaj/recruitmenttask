//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var detailItemModel: SingleItemModel? {
        didSet {
            DispatchQueue.main.async {
                self.textView.text = self.detailItemModel?.data?.attributes?.desc
                self.view.backgroundColor = self.detailItemModel?.data?.attributes?.color?.getColor() ?? .white
            }
        }
    }
    
    var itemModel: ItemModel? {
        didSet {
            guard let id = self.itemModel?.id,
                let filename = getFilename(id) else { print("EMPTY FILENAME OR ID"); return }
            self.provideDetailItem(filename)
        }
    }
    
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = itemModel?.attributes?.color?.getColor()
        self.title = getFormattedTitle(itemModel?.attributes?.name ?? "")
    }
    
    private func getFormattedTitle(_ title: String) -> String {
        var index: Int = 0
        var newTitle = ""
        title.forEach { char in
            newTitle += ((index == 0 || index == 2) ? String(char).uppercased() : String(char))
            index += 1
        }
        return newTitle
    }
 
    private func provideDetailItem(_ filename: FilenameProvider.Filename) {
        NetworkingManager.provide(filename: filename.rawValue, decodeTo: SingleItemModel()) { result in
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let itemModel):
                self.detailItemModel = itemModel
            }
        }
    }
    
    private func getFilename(_ id: String) -> FilenameProvider.Filename? {
        switch id {
        case "1":
            return FilenameProvider.Filename.item1
        case "2":
            return FilenameProvider.Filename.item2
        case "3":
            return FilenameProvider.Filename.item3
        case "4":
            return FilenameProvider.Filename.item4
        case "5":
            return FilenameProvider.Filename.item5
        default:
            return nil
        }
    }
}
