//
//  Attributes.swift
//  Recruitment-iOS
//
//  Created by mac on 22/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

// MARK: - Attributes
struct Attributes: Codable {
    
    let name: String?
    let preview: String?
    let color: ApiColors?
    let desc: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case preview = "preview"
        case color = "color"
        case desc = "desc"
    }
}
