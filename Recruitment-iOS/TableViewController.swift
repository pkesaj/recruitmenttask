//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var itemModels: ItemsModel? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NetworkingManager.provide(filename: FilenameProvider.Filename.items.rawValue, decodeTo: ItemsModel()) { [weak self] result in
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let items):
                self?.itemModels = items
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels?.data?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath)
        
        if itemModels?.data?.count ?? 0 > indexPath.row,
            let itemModel = itemModels?.data?[indexPath.row] {
            
            cell.backgroundColor = itemModel.attributes?.color?.getColor()
            cell.textLabel?.text = itemModel.attributes?.name
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = self.itemModels?.data?[indexPath.row] {
            
            guard let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as? DetailsViewController else { return }
            
            detailVC.itemModel = item
            
            self.navigationController?.pushViewController(detailVC, animated: true)

            
        }
    }
}
