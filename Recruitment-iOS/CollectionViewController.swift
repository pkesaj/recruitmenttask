//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by mac on 23/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {
    
    private weak var collectionView: UICollectionView?
    
    var itemModels: ItemsModel? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadData()
    }
    
    private func loadData() {
        NetworkingManager.provide(filename: FilenameProvider.Filename.items.rawValue, decodeTo: ItemsModel()) { [weak self] result in
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let items):
                self?.itemModels = items
            }
        }
    }
    
    private func setupCollectionView() {
        guard self.collectionView == nil else { return }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.view.addSubview(collectionView)
        self.collectionView = collectionView
       
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        registerCells()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        collectionView.reloadData()
    }
    
    private func registerCells() {
        collectionView?.register(CollectionCell.self, forCellWithReuseIdentifier: CollectionCell.identifier)
    }
}

extension CollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let item = self.itemModels?.data?[indexPath.row] {
            
            guard let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as? DetailsViewController else { return }
            
            detailVC.itemModel = item
            
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCell.identifier, for: indexPath)
        
        if itemModels?.data?.count ?? 0 > indexPath.row,
            let itemModel = itemModels?.data?[indexPath.row],
            let collectionCell = cell as? CollectionCell {
            
            collectionCell.model = itemModel.attributes
        }
        
        return cell
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CollectionCell.getCellSize(collectionView)
    }
}
