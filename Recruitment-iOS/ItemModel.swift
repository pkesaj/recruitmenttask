//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import Foundation

// MARK: - ItemModel
struct ItemModel: Codable {
    
    let id: String?
    let type: String?
    let attributes: Attributes?
    
    init() {
        id = nil
        type = nil
        attributes = nil
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case attributes = "attributes"
    }
}
