//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class NetworkingManager: NSObject {

    enum NetworkError: Error {
        case fileNotFound
        case decoding(Error)
    }
    
    static func provide<T: Decodable>(filename: String, decodeTo: T, completion: @escaping (Result<T, NetworkError>) -> (Void)) {
        NetworkingManager.request(filename, completionBlock: { (result) in
            
            switch result {
            case .success(let data):
                do {
                    let decodedData = try JSONDecoder().decode(T.self, from: data)
                    completion(Result.success(decodedData))
                } catch let error {
                    completion(Result.failure(NetworkError.decoding(error)))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        })
    }
    
    private static func getDataFromFile(_ fileName: String) -> Data? {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else { return nil }
        //to jest na jana zrobione ale nie potrzeba niczego wiecej
        return try? String(contentsOfFile: path, encoding: String.Encoding.utf8).data(using: .utf8)
    }
    
    private static func request(_ fileName : String, completionBlock: @escaping (Result<Data, NetworkError>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            guard let dataFromFile = NetworkingManager.getDataFromFile(fileName) else {
                completionBlock(Result.failure(NetworkError.fileNotFound))
                return
            }
            completionBlock(Result.success(dataFromFile))
        }
    }
}
