//
//  ApiColors.swift
//  Recruitment-iOS
//
//  Created by mac on 22/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

enum ApiColors: String, Codable {
    
    case black = "Black"
    case red = "Red"
    case green = "Green"
    case blue = "Blue"
    case yellow = "Yellow"
    case purple = "Purple"
    
    func getColor() -> UIColor {
        switch self {
        case .black: return .black
        case .blue: return .blue
        case .green: return .green
        case .purple: return .purple
        case .red: return .red
        case .yellow: return .yellow
        }
    }
}
